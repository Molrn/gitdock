const { test, expect } = require('@playwright/test');
const { newApp } = require('./util');

const addBookmark = async (a, link) => {
  await a.fill('#bookmark-link', link);
  await a.click('#bookmark-add-button');
};

test.describe('"Bookmarks" section', function () {
  test.describe('without bookmarks', () => {
    let electron;
    let window;
    test.beforeEach(async function () {
      electron = await newApp({
        loggedIn: true,
      });
      window = await electron.window;
    });

    test('can add and delete a bookmark', async function () {
      await addBookmark(window, 'https://gitlab.com/gitlab-org/gitlab/-/issues/1');

      const title = window.locator('#bookmark-title');
      await window.screenshot({
        path: 'test-results/screenshots/bookmarks/can-add-and-delete-a-bookmark-1.png',
        fullPage: true,
      });
      expect(await title.innerText()).toEqual('500 error on MR approvers edit page (#1)');

      await window.click('.bookmark-delete');

      const bookmarkInput = window.locator('#bookmark-link');
      await window.screenshot({
        path: 'test-results/screenshots/bookmarks/can-add-and-delete-a-bookmark-2.png',
        fullPage: true,
      });
      expect(await bookmarkInput.count()).toEqual(1);
      expect(await title.count()).toEqual(0);
    });
  });

  test.describe('with bookmarks', () => {
    const FIRST_BOOKMARK_URL = 'https://gitlab.com/user/project/-/merge_requests/1';

    let electron;
    let window;
    test.beforeEach(async function () {
      electron = await newApp({
        loggedIn: true,
        bookmarks: [
          {
            added: Date.now(),
            title: 'Test Merge Request (!1)',
            parent_name: 'Test Project',
            parent_url: 'https://gitlab.com/user/project',
            type: 'merge_request',
            web_url: FIRST_BOOKMARK_URL,
          },
        ],
      });
      window = await electron.window;
    });

    test('can delete a bookmark', async function () {
      const title = window.locator('#bookmark-title');
      await window.screenshot({
        path: 'test-results/screenshots/bookmarks/can-delete-a-bookmark-1.png',
        fullPage: true,
      });
      expect(await title.count()).toEqual(1);

      await window.click('.bookmark-delete');

      await window.screenshot({
        path: 'test-results/screenshots/bookmarks/can-delete-a-bookmark-2.png',
        fullPage: true,
      });
      expect(await title.count()).toEqual(0);
    });

    test('can add a bookmark', async function () {
      await window.click('#add-bookmark-dialog a');

      await addBookmark(window, 'https://gitlab.com/gitlab-org/gitlab/-/issues/1');

      const titles = window.locator('.bookmark-information a');
      await window.screenshot({
        path: 'test-results/screenshots/bookmarks/can-add-a-bookmark.png',
        fullPage: true,
      });
      expect(await titles.count()).toEqual(2);
    });

    test('cannot add a bookmark twice', async function () {
      await window.click('#add-bookmark-dialog a');

      await addBookmark(window, FIRST_BOOKMARK_URL);
      await window.screenshot({
        path: 'test-results/screenshots/bookmarks/cannot-add-a-bookmark-twice.png',
        fullPage: true,
      });
      expect(await window.textContent('#add-bookmark-error')).toEqual(
        'This bookmark has already been added.',
      );
    });
  });
});
