const { test, expect } = require('@playwright/test');
const { newApp } = require('./util');

test.describe('Favorite projects', function () {
  const EXAMPLE_PROJECT = {
    added: Date.now(),
    type: 'projects',
    web_url: 'https://gitlab.com/mvanremmerden/gitdock',
    id: 28462485,
    visibility: 'public',
    name: 'GitDock ⚓️',
    title: 'GitDock ⚓️',
    namespace: {
      name: 'Marcel van Remmerden',
    },
    parent_name: 'Marcel van Remmerden / GitDock ⚓️',
    parent_url: 'https://gitlab.com/mvanremmerden',
    name_with_namespace: 'Marcel van Remmerden / GitDock ⚓️',
    open_issues_count: 52,
    last_activity_at: '2022-01-31T09:55:37.993Z',
    avatar_url: 'https://gitlab.com/uploads/-/system/project/avatar/28462485/gitlab.png',
    star_count: 124,
    forks_count: 17,
  };

  const listFavoriteProjects = async (window) =>
    window.locator('[data-testid="favorite-projects"] li').allTextContents();

  const openSettings = async (window) => {
    await window.click('#edit-favorite-projects');
    await window.waitForSelector('#favorite-projects');
  };

  const addProject = async (window, url) => {
    await window.click('#add-project-dialog a');
    await window.fill('#project-settings-link', url);
    await window.click('#project-settings-add-button');
    await window.waitForSelector('#add-project-dialog a');
  };

  test.describe('without favorite project', function () {
    let electron;
    let window;
    test.beforeEach(async function () {
      electron = await newApp({
        loggedIn: true,
      });
      window = electron.window;
    });

    test('shows no favorite projects', async function () {
      await window.screenshot({
        path: 'test-results/screenshots/favorite-projects/shows-no-favorite-projects.png',
        fullPage: true,
      });
      expect((await listFavoriteProjects(window)).length).toEqual(0);
    });

    test('adds a new project', async function () {
      await openSettings(window);
      await addProject(window, EXAMPLE_PROJECT.web_url);

      await window.click('#detail-header');
      await window.waitForSelector('[data-testid="favorite-projects"]');

      const projects = await listFavoriteProjects(window);

      await window.screenshot({
        path: 'test-results/screenshots/favorite-projects/adds-a-new-favorite-project.png',
        fullPage: true,
      });
      expect(projects.length).toEqual(1);
      expect(projects[0].includes(EXAMPLE_PROJECT.name)).toBe(true);
      expect(projects[0].includes(EXAMPLE_PROJECT.namespace.name)).toBe(true);
    });
  });

  test.describe('with favorite project', function () {
    let electron;
    let window;
    test.beforeEach(async function () {
      electron = await newApp({
        loggedIn: true,
        favoriteProjects: [EXAMPLE_PROJECT],
      });
      window = electron.window;
    });

    test('shows the correct project link', async function () {
      const onclick = await window
        .locator('[data-testid="favorite-projects"] li')
        .getAttribute('onclick');
      await window.screenshot({
        path: 'test-results/screenshots/favorite-projects/shows-the-correct-project-link.png',
        fullPage: true,
      });
      expect(onclick).toEqual(`goToDetail('Project', '${JSON.stringify(EXAMPLE_PROJECT)}')`);
    });

    test('deletes an existing project', async function () {
      await window.screenshot({
        path: 'test-results/screenshots/favorite-projects/deletes-an-existing-project-1.png',
        fullPage: true,
      });
      expect((await listFavoriteProjects(window)).length).toEqual(1);

      await window.click('#edit-favorite-projects');
      await window.waitForSelector('#favorite-projects');
      await window.click('#favorite-projects .bookmark-delete');

      await window.click('#detail-header');
      await window.waitForTimeout(100);

      await window.screenshot({
        path: 'test-results/screenshots/favorite-projects/deletes-an-existing-project-2.png',
        fullPage: true,
      });
      expect((await listFavoriteProjects(window)).length).toEqual(0);
    });

    test('prevents adding a project twice', async function () {
      await openSettings(window);

      await window.click('#add-project-dialog a');
      await window.fill('#project-settings-link', EXAMPLE_PROJECT.web_url);
      await window.click('#project-settings-add-button');
      await window.waitForSelector('#add-project-settings-error');

      const error = window.locator('#add-project-settings-error');

      await window.screenshot({
        path: 'test-results/screenshots/favorite-projects/prevents-adding-a-project-twice-1.png',
        fullPage: true,
      });
      expect(await error.innerText()).toEqual('The same project was already added.');

      await window.click('#detail-header');
      await window.waitForTimeout(100);

      await window.screenshot({
        path: 'test-results/screenshots/favorite-projects/prevents-adding-a-project-twice-2.png',
        fullPage: true,
      });
      expect((await listFavoriteProjects(window)).length).toEqual(1);
    });
  });
});
